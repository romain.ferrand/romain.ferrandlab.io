---
# Display name
title: Romain Ferrand

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Phd student in Computer Science / Neuro-informatics

# Organizations/Affiliations
organizations:
- name: IGI Team, TU Graz, Austria
  url: "https://www.tugraz.at/institute/igi/people/team/"
- name: ENS de Rennes
  url: "http://www.ens-rennes.fr/"
- name: Université Rennes 1
  url: "https://www.univ-rennes1.fr/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include neurosciences, decision-making and reinforcement learning.

interests:
- Computational Neurosciences
- Cognitive Neuroscience
- Cognitive Psychology
- Reinforcement learning

education:
  courses:
  - course: Phd student in Computer science
    institution: TU Graz, Austria
  - course: Msc in Computer Science
    institution: ENS de Rennes, France
    year: 2020
  - course: BSc in Computer Science
    institution: ENS de Rennes, France
    year: 2018
  - course: BSc in mathematics and Computer Science
    institution: Nantes, France
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:romain.ferrand@igi.tugraz.at"  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: gitlab
#  icon_pack: fab
#  link: https://gitlab.com/romain.ferrand
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
#- icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "romain.ferrand@ens-rennes.fr"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
#user_groups:
#- Researchers
# - Visitors

---

Romain Ferrand is a Phd student in computer science / neuro-informatics  at Institute of Theoretical Computer Science, TU Graz, Austria
