---
title: "Investigation of the successor representation for neuro-computational models of the hippocampus"
date: 2019-08-30T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors: ["admin"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Preprint / Working Paper
# 4 = Report
# 5 = Book
# 6 = Book section
# 7 = Thesis
# 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated version.
publication: "Investigation of the successor representation for neuro-computational models of the hippocampus"
publication_short: ""

# Abstract.
abstract: "Successor representations (SR) is a novel trade-off between model-free and model-based reinforcement learning (RL) which has been used to explain several neurobiological findings, including dopaminergic reward prediction errors. (Stachenfeld & al. 2017) showed that it can be used to explain the formation of place and grid cells in the hippocampal. This paper re-implement this model and provide a new multi-timescale algorithm of the SR along with a neurobiologically feasible actor-critic framework. Finally we study the properties of these models in the context of a Tolman’s maze, goal-directed experiments and its capacity of automatically find subgoals using clustering."

# Summary. An optional shortened abstract.
summary: ""

# Digital Object Identifier (DOI)
doi: ""

# Is this a featured publication? (true/false)
featured: false

# Tags (optional).
#   Set `tags: []` for no tags, or use the form `tags: ["A Tag", "Another Tag"]` for one or more tags.
tags: ["cognitive neurosciences", "hippocampus", "reinforcement learning", "successor representation", "multi-time scale SR", "place-cells"]


# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects: ["deep-learning"]` references
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example-slides"` references
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides: ""`.
slides: ""

# Links (optional).
url_pdf: "/articles/investigation_hippocampus_successor_representation.pdf"
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_video: ""
url_poster: ""
url_source: ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# links: [{name: "Custom Link", url: "http://example.org"}]

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---
