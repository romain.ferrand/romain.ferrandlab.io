---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Episodic Memory Augmented RL"
authors: []
date: 2020-08-22T12:32:35+02:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2020-08-22T12:32:35+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: "Episodic Memory Augmented RL"
publication_short: ""

abstract: "
Animal decision-making falls into two behavioral categories, habits involving reflex and instinctive decisions and goal directed behavior entailing planning and world-model representations.
	*Hippocampus* (HPC) embedded within a large neural network is supposed to play a major role in goal-directed behaviors; one aspect of HPC is its ability to memorize, recall and replay sequences of experienced events while being able to generate novels ones. This capacity can be summarized thought the name of *episodic memory*.
	The evaluation of sequences in terms of possible future outcomes allows HPC to generate goal-directed behaviors while maintaining a partial representation of the environment [pezzulo2014internally].
	This work review the current neurosciences theories linking *episodic memory* and decision-making,
	We then focus on previous studies [momennejad_predicting_2018] proposing reinforcement learning (RL), a standard learning framework, as a way to formalize and model *episodic memory*.
	These different works lead us to think that RL is interesting framework for providing computational theories of *episodic memory*, and studying *episodic memory* may lead in return to valuable bio-inspired algorithms. However, those models are still proof of concept techniques evaluated on grid-worlds environments,
	we propose an *episodic memory* implementation working on an arbitrary observations space by providing end-to-end deep-RL model and techniques to generate, evaluate and recall sequences of behaviors.
	Finally, we provide virtual environments used to test and validate the model against experimental data.
"

# Summary. An optional shortened abstract.
summary: ""

tags: ["episodic memory", "HPC", "deep-RL", "TCM", "SR","decision-making"]
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "/articles/episodic_memory_augemented_rl.pdf"
url_code: "https://gitlab.com/romain.ferrand/multi-time-scale-sr"
url_dataset:
url_poster:
url_project:
url_slides: "/slides/FERRAND_episodic_memory_augmented_RL_internship.pdf"
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
